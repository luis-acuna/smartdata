import tensorflow as tf
import keras_preprocessing
from tensorflow.keras.applications.inception_v3 import InceptionV3
from keras_preprocessing import image
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, GlobalAveragePooling2D, Input, Dropout
from tensorflow.keras.optimizers import Adam, SGD
from keras_preprocessing.image import ImageDataGenerator

config_training_directory = "FIRE-SMOKE-DATASET/Train/"
config_test_directory = "FIRE-SMOKE-DATASET/Test/"

# InceptionV3 CNN Model for Fire Detection
def create_inceptionV3_model(training_directory, test_directory):
    training_datagen = ImageDataGenerator(rescale = 1./255,
                                    horizontal_flip=True,
                                    zoom_range=0.15,
                                    height_shift_range=0.2,
                                    fill_mode='nearest')
    
   
    validation_datagen = ImageDataGenerator(rescale = 1./255)

    train_generator = training_datagen.flow_from_directory(training_directory,
                                            target_size=(224,224),
                                            shuffle=True,
                                            class_mode='categorical',
                                            batch_size = 128)
    validation_generator = validation_datagen.flow_from_directory(      
                                            test_directory,
                                            target_size=(224,224),
                                            class_mode='categorical',
                                            batch_size= 16)
    
    input_tensor = Input(shape=(224, 224, 3))
    base_model = InceptionV3(input_tensor=input_tensor, weights='imagenet', include_top=False)
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(2048, activation='relu')(x)
    x = Dropout(0.25)(x)
    x = Dense(1024, activation='relu')(x)
    x = Dropout(0.2)(x)

    predictions = Dense(2, activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=predictions)
    for layer in base_model.layers:
        layer.trainable = False
    model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['acc'])

    model.summary()
    
    history = model.fit(
        train_generator,
        steps_per_epoch = 14,
        epochs = 20,
        validation_data = validation_generator,
        validation_steps = 14)
    
    model.save("./saved_models/inceptionV3_trained_cnn")
    

# AlexNet CNN Model for Fire Detection
def create_alexnet_model(training_directory, test_directory):
    training_datagen = ImageDataGenerator(rescale = 1./255,
                                    horizontal_flip=True,
                                    rotation_range=30,
                                    height_shift_range=0.2,
                                    fill_mode='nearest')
    
   
    validation_datagen = ImageDataGenerator(rescale = 1./255)

    train_generator = training_datagen.flow_from_directory(training_directory,
                                            target_size=(224,224),
                                            class_mode='categorical',
                                            batch_size = 64)
    validation_generator = validation_datagen.flow_from_directory(      
                                            test_directory,
                                            target_size=(224,224),
                                            class_mode='categorical',
                                            batch_size= 16)
    
    model = tf.keras.models.Sequential([
            tf.keras.layers.Conv2D(96, (11,11), strides=(4,4), activation='relu', input_shape=(224, 224, 3)), 
            tf.keras.layers.MaxPooling2D(pool_size = (3,3), strides=(2,2)),
            tf.keras.layers.Conv2D(256, (5,5), activation='relu'),
            tf.keras.layers.MaxPooling2D(pool_size = (3,3), strides=(2,2)),
            tf.keras.layers.Conv2D(384, (5,5), activation='relu'),
            tf.keras.layers.MaxPooling2D(pool_size = (3,3), strides=(2,2)),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Dense(2048, activation='relu'),
            tf.keras.layers.Dropout(0.25),
            tf.keras.layers.Dense(1024, activation='relu'),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Dense(2, activation='softmax')
            ])

    model.compile(loss='categorical_crossentropy',
        optimizer=Adam(learning_rate=0.0001),
        metrics=['acc'])

    model.summary()
    
    history = model.fit(
        train_generator,
        steps_per_epoch = 15,
        epochs = 50,
        validation_data = validation_generator,
        validation_steps = 15
        )

    #To train the top 2 inception blocks, freeze the first 249 layers and unfreeze the rest.
    for layer in model.layers[:249]:
        layer.trainable = False
    
    for layer in model.layers[249:]:
        layer.trainable = True
    
    #Recompile the model for these modifications to take effect
    model.compile(optimizer=SGD(learning_rate=0.0001, momentum=0.9),
         loss='categorical_crossentropy', metrics=['acc'])

    history = model.fit(
        train_generator,
        steps_per_epoch = 14,
        epochs = 10,
        validation_data = validation_generator,
        validation_steps = 14)

    model.save("./saved_models/alexnet_trained_cnn")

if __name__ == '__main__':
    create_inceptionV3_model(config_training_directory, config_test_directory)
