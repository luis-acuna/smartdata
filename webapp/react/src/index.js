import React
, { Component } from "react";
import ReactDOM from "react-dom";
import "./styles.css";
import Map from "./Map";
class Datas extends Component {
state = {
datas: [],
loading: true
};
componentDidMount() {
  fetch('http://localhost:8080/',
  {
    method: "GET",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    mode:'cors',
  })
  .then((response) => response.json())
  .then((datas) => {
    this.setState({
      loading: false,
      datas
      });
      });
    }
    render() {
      console.log("hola");
      console.log(this.state.datas);
      return  <Map places={this.state.datas} center={{ lat: 36.529527, lng: -6.29241 }} />;
    }
}
function App() {
  return (
  <div className="App">
  <Datas />
  </div>
  );
  }
const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);