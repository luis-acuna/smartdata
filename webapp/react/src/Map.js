/* global google */
import React, { Component } from "react";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import InfoWindowEx from "./InfoWindowEx";

const postUrl = 'https://gorest.co.in/public/v2/users';
const iconBase =
"./";
const icons = {
fire: {
  icon: iconBase + "fire.png",
},
rain: {
  icon: iconBase + "rain.png",
},
};
export class MapContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {}
    };
  }
  
  onMarkerClick = (props, marker, e) => {
    this.setState({
      selectedPlace: props.place_,
      activeMarker: marker,
      showingInfoWindow: true
    });
  };

  postThereIsFire = place => {
    console.log(place);
    const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ image: place.image, lat: place.lat, lng: place.lng, label: "fire" })
    };
    fetch(postUrl, requestOptions)
    .then(response => response.json())
    ;
  };
  postThereIsNoFire = place => {
    console.log(place);
    const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ image: place.image, lat: place.lat, lng: place.lng, label: "nofire" })
    };
    fetch(postUrl, requestOptions)
    .then(response => response.json())
    ;
  };

  render() {
    return (
      <div className="map-container">
        <Map
          google={this.props.google}
          className={"map"}
          zoom={14}
          initialCenter={this.props.center}
        >
          {this.props.places.map((place, i) => {
            return (
              <Marker
                onClick={this.onMarkerClick}
                key={place.id}
                place_={place}
                icon={icons[place.type].icon}
                position={{ lat: place.lat, lng: place.lng }}
              />
            );
          })}
          <InfoWindowEx
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow && this.state.selectedPlace.type === "fire"}
          >
            <div>
              <h3>{this.state.selectedPlace.name}</h3>
              Ayuda con tu información!<br/>
              <button
                type="button"
                onClick={this.postThereIsFire.bind(this, this.state.selectedPlace)}
              >
                Confirmar incendio
              </button>
              <button
                type="button"
                onClick={this.postThereIsNoFire.bind(this, this.state.selectedPlace)}
              >
                No hay incendio
              </button>
            </div>
          </InfoWindowEx>
        </Map>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyDRNGshNc2TGetD8AvQod3p49AsW0CueN4"
})(MapContainer);
