from google.cloud import language
from flask import Flask, request, render_template, Response
from database import db
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

db = db()
@app.route('/', methods=['GET'])
def get_all():
    return Response(json.dumps(db.get_all_nminutes(290)),  mimetype='application/json')
@app.route('/fire', methods=['GET'])
def fire_get():
    return Response(json.dumps(db.get_fire_nminutes(290)),  mimetype='application/json')
@app.route('/rain', methods=['GET'])
def rain_get():
    return Response(json.dumps(db.get_rain_nminutes(290)),  mimetype='application/json')

@app.route('/fire', methods=['POST'])
def fire_post():
    print(request.json)

    db.fire_post(request.json["image"], request.json["lat"], request.json["lng"], request.json["typex"])
    return "OK"
@app.route('/rain', methods=['POST'])
def rain_post():
    db.rain_post(request.json["image"], request.json["lat"], request.json["lng"], request.json["typex"])
    return "OK"
