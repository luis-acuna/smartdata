import psycopg2
import psycopg2.extras
from random import randint
import sys
import copy
import time

locations=[{
  "lat": 36.51923534017225,
  "lng": -6.276910508663393
},
{
  "lat": 36.5170969788607,
  "lng": -6.285407746822573
},
{
  "lat": 36.50273714986985,
  "lng": -6.276025772094727
},
{
  "lat": 36.50709418167759,
  "lng": -6.273219789058901
},
{
  "lat": 36.51268211051373,
  "lng": -6.272790635616518
},
{
  "lat": 36.51350991751326,
  "lng": -6.280687058956362
},
{
  "lat": 36.51633819128771,
  "lng": -6.2787987838098775
},
{
  "lat": 36.52261521010775,
  "lng": -6.281287873775698
},
{
  "lat": 36.525719042233504,
  "lng": -6.285150254757143
},
{
  "lat": 36.528546869752695,
  "lng": -6.289956773311831
},
{
  "lat": 36.533443594841295,
  "lng": -6.295621598751284
},
{
  "lat": 36.532340134557344,
  "lng": -6.301629746944643
},
{
  "lat": 36.53019155316653,
  "lng": -6.303062438964844
},
{
  "lat": 36.53019155316653,
  "lng": -6.303062438964844
},
{
  "lat": 36.52951244571685,
  "lng": -6.298797334224917
},
{
  "lat": 36.52902965924148,
  "lng": -6.29347583153937
},
{
  "lat": 36.52302906158609,
  "lng": -6.2862660537073385
},
{
  "lat": 36.5219944287394,
  "lng": -6.283261979610659
},
{
  "lat": 36.512751094768525,
  "lng": -6.277339662105776
},
{
  "lat": 36.51068154037725,
  "lng": -6.27424975732062
},
{
  "lat": 36.511302412505145,
  "lng": -6.279828752071596
},
{
  "lat": 36.50702519238095,
  "lng": -6.269700730831362
},
{
  "lat": 36.50329967905963,
  "lng": -6.2705590377161275
},
{
  "lat": 36.501574843660485,
  "lng": -6.2733056197473775
},
{
  "lat": 36.499504990460295,
  "lng": -6.271331513912417
},
{
  "lat": 36.49577911526646,
  "lng": -6.26849910119269
},
{
  "lat": 36.4944681165454,
  "lng": -6.26549502709601
},
{
  "lat": 36.50005695672386,
  "lng": -6.263349259884096
},
{
  "lat": 36.50316169364178,
  "lng": -6.266353333980776
},
{
  "lat": 36.50039134727702,
  "lng": -6.256113052368164
},
{
  "lat": 36.4977800705266,
  "lng": -6.271074021846987
},
{
  "lat": 36.52137364239007,
  "lng": -6.245410645992495
},
{
  "lat": 36.523167011586914,
  "lng": -6.248758042843081
},
{
  "lat": 36.52213238058505,
  "lng": -6.227128709346987
},
{
  "lat": 36.51344093393483,
  "lng": -6.240089143306948
},
{
  "lat": 36.49288108840086,
  "lng": -6.266267503292299
}]
class Postgres(object):
    def __init__(self, dbname="postgres", user="postgres",password="example", host="localhost", port="5432"):
        self.connect = psycopg2.connect("dbname='"+dbname+"' user='"+user+"' password='"+password+"' host='"+host+"' port='"+port+"'")
        self.connect.autocommit = True
    def conn(self):
        return self.connect
    def fire_post(self, image, lat, lng, typex):
        connection = self.conn()
        cursor = connection.cursor()
        location_zone = randint(0,35)

        q = "INSERT INTO public.fire(event_date, image, lat, lng, typex) VALUES( NOW(), '"+str(image)+"',"+str(locations[location_zone]["lat"])+","+str(locations[location_zone]["lng"])+",'"+typex+"')"
        print(q)
        try:
            cursor.execute(q)
        except:
            cursor.close()
            print("Unexpected error:", sys.exc_info()[0])
            return None
        cursor.close()
    def rain_post(self, image, lat, lng, typex):
        connection = self.conn()
        cursor = connection.cursor()
        location_zone = randint(0,35)

        q = "INSERT INTO public.rain(event_date, image, lat, lng, typex) VALUES( NOW(), '"+str(image)+"',"+str(locations[location_zone]["lat"])+","+str(locations[location_zone]["lng"])+",'"+typex+"')"
        print(q)
        try:
            cursor.execute(q)
        except:
            cursor.close()
            print("Unexpected error:", sys.exc_info()[0])
            return None
        cursor.close()
    def get_fire_nminutes(self, nminutes):
        connection = self.conn()
        cur = connection.cursor()
        q = "SELECT * FROM public.fire where event_date>NOW() - INTERVAL '"+str(nminutes)+" MINUTE' order by event_date ASC"
        res = []
        try:
            cur.execute(q)
            desc = cur.description
            rows = cur.fetchall()
            i=0
            for row in rows:
                det={}
                r=0
                for col in desc:
                    if col[0] == "typex":
                        det.update({"type":str(row[r])})
                    else:
                        det.update({col[0]:str(row[r])})
                    r=r+1
                res.append(copy.deepcopy(det))
                i=i+1
        except:
            cur.close()
            return {}
        cur.close()
        print(res)
        return res
    def get_rain_nminutes(self, nminutes):
        connection = self.conn()
        cur = connection.cursor()
        q = "SELECT * FROM public.rain where event_date>NOW() - INTERVAL '"+str(nminutes)+" MINUTE' order by event_date ASC"
        res = []
        try:
            cur.execute(q)
            desc = cur.description
            rows = cur.fetchall()
            i=0
            for row in rows:
                det={}
                r=0
                for col in desc:
                    if col[0] == "typex":
                        det.update({"type":str(row[r])})
                    else:
                        det.update({col[0]:str(row[r])})
                    r=r+1
                res.append(copy.deepcopy(det))
                i=i+1
        except:
            cur.close()
            return {}
        cur.close()
        print(res)
        return res
    def get_all_nminutes(self, nminutes):
        connection = self.conn()
        cur = connection.cursor()
        q = "SELECT * FROM public.rain where event_date>NOW() - INTERVAL '"+str(nminutes)+" MINUTE' UNION SELECT * FROM public.fire where event_date>NOW() - INTERVAL '"+str(nminutes)+" MINUTE' order by event_date ASC"
        res = []
        try:
            cur.execute(q)
            desc = cur.description
            rows = cur.fetchall()
            i=0
            for row in rows:
                det={}
                r=0
                for col in desc:
                    if col[0] == "typex":
                        det.update({"type":str(row[r])})
                    else:
                        if col[0] != "event_date":
                            det.update({col[0]:row[r]})
                        else:
                            pass
                    r=r+1
                res.append(copy.deepcopy(det))
                i=i+1
        except:
            cur.close()
            return {}
        cur.close()
        print(res)
        return res


        
class db(Postgres):
    pass
