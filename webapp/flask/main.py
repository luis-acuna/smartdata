import requests
from database import db
from bs4 import BeautifulSoup
from sentiment_analysis_spanish import sentiment_analysis

import requests
import random

cookies = {
    'pxi': '290fb58e39b973887752123b8be38cf6',
    '__utma': '202678606.1116731276.1612174656.1612174656.1613421954.2',
    '__utmc': '202678606',
    '__utmz': '202678606.1613421954.2.2.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided)',
    '__utmt': '1',
    '__utmt_o': '1',
    '__utmt_f': '1',
    '__utmb': '202678606.26.7.1613422071742',
}

headers = {
    'Connection': 'keep-alive',
    'sec-ch-ua': '"Google Chrome";v="87", " Not;A Brand";v="99", "Chromium";v="87"',
    'Accept': '*/*',
    'X-Requested-With': 'XMLHttpRequest',
    'sec-ch-ua-mobile': '?0',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Dest': 'empty',
    'Referer': 'https://www.soychile.cl/',
    'Accept-Language': 'en,es-US;q=0.9,es-419;q=0.8,es;q=0.7',
    'If-Modified-Since': 'Mon, 15 Feb 2021 21:16:21 GMT',
}
i=0
for ndiario in range(30):
    params = (
        ('idDiario', str(ndiario)),
        ('fromSoyChile', 'true'),
        ('cantidad', '100')
    )
    datab = db()
    base_url='https://www.soychile.cl'
    url=base_url+'/noticias/json/nuevos/jsonMasVistas.aspx'
    r = requests.get(url, headers=headers, params=params, cookies=cookies)
    url_word = ''
    principal_words = ["alcalde"]
    words=""
    hash = random.getrandbits(128)
    for word in principal_words:
        words=words+","+word
    for noticia in r.json()['masVistas']:
        if url_word.upper() not in noticia['url'].upper():
            continue
        print(noticia['titulo'])
        print(base_url+noticia['url'].upper())
        specific_url=base_url+noticia['url']
        r2 = requests.get(base_url+noticia['url'], headers=headers, cookies=cookies)
        soup = BeautifulSoup(r2.text, "html.parser")
        heading = soup.find("div", {"class": "note-inner-heading"}).text.strip().replace('\n', '. ')
        text= soup.find("div", {"class": "note-inner-text"}).text.strip().replace('\n', '. ')
        relevant = False
        for word in principal_words:
            relevant = word.upper() in heading.upper() or word.upper() in text.upper()
            if relevant:
                print("There is match with relevant word")
                sentiment = sentiment_analysis.SentimentAnalysisSpanish()
                datab.insert_data( hash, specific_url, words, noticia['titulo'],heading, text, noticia['timestamp'], sentiment.sentiment(noticia['titulo']))
                break
        i+=1
        print(i)
