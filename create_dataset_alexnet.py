# organize dataset into a useful structure
from os import makedirs
from os import listdir
from shutil import copyfile
from random import seed
from random import random

if __name__ == '__main__':
    # create directories
    dataset_home = 'splitted_fire_dataset/'
    subdirs = ['train/', 'test/']
    for subdir in subdirs:
        # create label subdirectories
        labeldirs = ['non_fire/', 'fire/']
        for labldir in labeldirs:
            newdir = dataset_home + subdir + labldir
            makedirs(newdir, exist_ok=True)
    # seed random number generator
    seed(1)
    # define ratio of pictures to use for validation
    val_ratio = 0.25
    # copy training dataset images into subdirectories
    src_directory = 'full_fire_dataset/'
    for file in listdir(src_directory):
        src = src_directory + '/' + file
        dst_dir = 'train/'
        if random() < val_ratio:
            dst_dir = 'test/'
        if file.startswith('non_fire'):
            dst = dataset_home + dst_dir + 'non_fire/'  + file
            copyfile(src, dst)
        elif file.startswith('fire'):
            dst = dataset_home + dst_dir + 'fire/'  + file
            copyfile(src, dst)